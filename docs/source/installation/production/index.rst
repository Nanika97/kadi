.. _installation-production:

Production
==========

.. note::
    The productive installation has currently been tested under the following
    Debian-based Linux distributions, which all instructions and provided scripts are
    based on:

    * **Debian 10** (Buster)
    * **Debian 11** (Bullseye)
    * **Ubuntu 20.04 LTS** (Focal Fossa)
    * **Ubuntu 22.04 LTS** (Jammy Jellyfish)

    It is generally recommended to use the latest version of the respective
    distribution.

This section describes how to install, :ref:`update <installation-production-updating>`
and :ref:`backup <installation-production-backup>` |kadi| in a production environment
using a simple, single-machine setup. This type of setup should already be sufficient
for most cases to install a |kadi| instance for light to medium use.

The required hardware for this setup may vary for each use case, especially related to
storage for the database, the search index and locally stored files. As a general rule
of thumb for RAM and CPU, a minimum of **16 GB RAM** and a decent **CPU with 8 cores**
should be sufficient for this type of setup.

.. warning::
    Keep in mind that, for production deployments, there are other aspects to consider
    as well, e.g. securing the server the application and services are running on by
    keeping dependent packages up to date, using a firewall, etc.

There are currently two ways to perform the installation: installation via :ref:`setup
script <installation-production-script>` and :ref:`manual
<installation-production-manual>` installation. When using the former, it is still
recommended to check out the manual installation instructions, as all the necessary
steps are described in much more detail, which is also recommended when planning to
deploy a more intricate, multi-machine setup.

.. note::
    After the installation, please make sure to at least check out the
    :ref:`configuration <installation-configuration>` reference as well, which explains
    how to further configure and customize |kadi|, especially with regard to user
    authentication.

.. toctree::
    :maxdepth: 1

    script
    manual
    updating
    backup
    python
