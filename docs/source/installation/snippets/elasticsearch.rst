`Elasticsearch <https://www.elastic.co/elasticsearch>`__ is the full-text search engine
used in the application. Currently, only version **7** is supported, which can be
installed like this:

.. code-block:: bash

    sudo apt install wget apt-transport-https gnupg
    wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo gpg --dearmor -o /usr/share/keyrings/elasticsearch-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-7.x.list
    sudo apt update && sudo apt install elasticsearch

Some configuration values have to be set manually after installation by using the
following command. These settings configure Elasticsearch to use a single-node cluster
and disable the basic security features included in free Elasticsearch installations,
which are not necessarily needed in this simple setup.

.. code-block:: bash

    echo -e "discovery.type: single-node\nxpack.security.enabled: false" | sudo tee -a /etc/elasticsearch/elasticsearch.yml

To start Elasticsearch and also configure it to start automatically when the system
boots, the following commands can be used:

.. code-block:: bash

    sudo systemctl enable elasticsearch.service
    sudo systemctl start elasticsearch.service

.. note::
    If the Elasticsearch service has trouble starting in time, try increasing the start
    timeout from 90 seconds to 180 seconds:

    .. code-block:: bash

        sudo mkdir /etc/systemd/system/elasticsearch.service.d
        echo -e "[Service]\nTimeoutStartSec=180" | sudo tee /etc/systemd/system/elasticsearch.service.d/startup-timeout.conf
        sudo systemctl daemon-reload
        sudo systemctl restart elasticsearch.service
