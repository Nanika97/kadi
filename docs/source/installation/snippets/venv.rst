.. note::
    If not using the default Python version installed via APT, virtual environments need
    to be created in a slightly different way, since the virtualenv version installed
    via APT may not work properly with the newly installed Python version. Instead, the
    built-in ``venv`` module can be used:

    .. code-block:: bash

        python3.x -m venv <my_venv>   # Instead of "virtualenv -p python3 <my_venv>"
        source <my_venv>/bin/activate
        pip install wheel
        pip install -U pip

    Note that the ``wheel`` package needs to be installed separately after activating
    the virtual environment for the first time, while updating pip ensures that its most
    recent version is used, both of which virtualenv normally would have done
    automatically.
