.. _apiref-plugins:

Plugins
=======

This section contains a complete API reference of the :mod:`kadi.plugins` Python module,
including the hook specifications that currently exist for use in plugins. For
implementing any of the plugin hooks, see also how to :ref:`develop plugins
<development-plugins>`.

.. _apiref-plugins-hooks:

Hooks
-----

.. automodule:: kadi.plugins.spec
    :members:
    :exclude-members: kadi_get_storages

Core
----

.. automodule:: kadi.plugins.core
    :members:

Utils
-----

.. automodule:: kadi.plugins.utils
    :members:
