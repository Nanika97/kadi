# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.revisions.core import create_revision
from kadi.lib.revisions.core import delete_revisions
from kadi.lib.revisions.models import Revision
from kadi.lib.revisions.schemas import ObjectRevisionSchema
from kadi.lib.revisions.utils import get_revision_columns
from kadi.modules.records.core import update_record
from kadi.modules.records.models import Record
from kadi.modules.records.schemas import RecordRevisionSchema


def test_create_revision(db, dummy_record, dummy_user):
    """Test if revisions are created correctly."""

    # Initial revision.
    first_revision = dummy_record.ordered_revisions.first()

    assert dummy_record.revisions.count() == 1
    assert dummy_record.revision_class == first_revision.__class__
    assert first_revision.model_class == dummy_record.__class__
    assert first_revision.revision.user_id == dummy_user.id
    assert first_revision.parent is None

    # Try to create a revision without any changes.
    create_revision(dummy_record, user=dummy_user)
    db.session.commit()

    assert dummy_record.revisions.count() == 1

    # Try to create a revision with actual changes.
    identifier = tag = "test"
    dummy_record.identifier = identifier
    dummy_record.set_tags([tag])

    create_revision(dummy_record, user=dummy_user)
    db.session.commit()

    second_revision = dummy_record.ordered_revisions.first()

    assert dummy_record.revisions.count() == 2
    assert dummy_record.revision_class == second_revision.__class__
    assert first_revision.child == second_revision
    assert second_revision.model_class == dummy_record.__class__
    assert second_revision.revision.user_id == dummy_user.id
    assert second_revision.parent == first_revision
    assert second_revision.title == dummy_record.title
    assert second_revision.identifier == identifier
    assert second_revision.tags == [{"name": tag}]


def test_delete_revisions(db, dummy_record):
    """Test if deleting revisions works correctly."""
    base_revision_id = dummy_record.ordered_revisions.first().revision_id
    delete_revisions(dummy_record)
    db.session.commit()

    assert not dummy_record.revisions.all()
    assert not Revision.query.get(base_revision_id)


def test_object_revision_schema(dummy_record, dummy_user):
    """Test if the object revision schema works correctly."""
    update_record(dummy_record, tags=["test"], user=dummy_user)

    revision = dummy_record.ordered_revisions.first()

    schema = ObjectRevisionSchema(
        RecordRevisionSchema,
        api_endpoint="api.get_record_revision",
        view_endpoint="records.view_record_revision",
        endpoint_args={"record_id": dummy_record.id},
    )
    data = schema.dump(revision)

    assert data["object_id"] == dummy_record.id
    assert data["data"]["tags"] == ["test"]
    assert data["diff"] == {"tags": {"new": ["test"], "prev": []}}

    # Try to compare the revision with itself.
    schema = ObjectRevisionSchema(RecordRevisionSchema, compared_revision=revision)
    data = schema.dump(revision)

    assert data["diff"] == {}


def test_get_revision_columns(monkeypatch):
    """Test if revisioned columns are parsed correctly."""
    monkeypatch.setattr(Record.Meta, "revision", ["test", "foo[ bar, baz ]"])

    assert get_revision_columns(Record) == (["test"], [("foo", ["bar", "baz"])])
