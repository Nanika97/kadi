# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import current_app

from kadi.lib.utils import has_capabilities


@pytest.mark.parametrize(
    "capabilities,result",
    [
        (["a"], True),
        (["a", "b"], True),
        (["a", "b", "c"], True),
        (["d"], False),
        (["a", "d"], False),
        (["a", "b", "c", "d"], False),
    ],
)
def test_has_capabilities(capabilities, result, monkeypatch, request_context):
    """Test if capabilities are determined correctly."""
    monkeypatch.setitem(current_app.config, "CAPABILITIES", ["a", "b", "c"])
    assert has_capabilities(*capabilities) is result
