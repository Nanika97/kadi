# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from marshmallow import ValidationError
from werkzeug.exceptions import HTTPException

from kadi.lib.conversion import lower
from kadi.lib.conversion import strip
from kadi.lib.schemas import check_duplicate_identifier
from kadi.lib.schemas import FilteredString
from kadi.lib.schemas import SortedPluck
from kadi.lib.tags.schemas import TagSchema
from kadi.modules.records.models import Record
from kadi.modules.records.schemas import RecordSchema


@pytest.mark.parametrize(
    "data,is_valid",
    [(None, False), ({"title": "test"}, False), ({"identifier": "test"}, True)],
)
def test_kadi_schema(data, is_valid, request_context):
    """Test if the custom schema base class works correctly."""
    schema = RecordSchema(only=["identifier"])
    assert not schema._internal

    if is_valid:
        schema.load_or_400(data)
    else:
        with pytest.raises(HTTPException):
            schema.load_or_400(data)


@pytest.mark.parametrize(
    "data,allow_ws_only,filters,result",
    [
        (None, False, [], None),
        (" ", False, [], None),
        ("test", False, [], "test"),
        (" test ", False, [], " test "),
        (" Test ", False, [lower, strip], "test"),
        ("", True, [], ""),
        (" ", True, [], " "),
        (" ", True, [strip], ""),
    ],
)
def test_filtered_string(data, allow_ws_only, filters, result):
    """Test if the custom "FilteredString" field works correctly."""
    field = FilteredString(allow_ws_only=allow_ws_only, filters=filters)

    if result is not None:
        assert field.deserialize(data) == result
    else:
        with pytest.raises(ValidationError):
            FilteredString().deserialize(data)


@pytest.mark.parametrize(
    "data,many,result",
    [
        (None, False, None),
        ({"name": "test"}, False, "test"),
        ([{"name": "b"}, {"name": "a"}, {"name": "c"}], True, ["a", "b", "c"]),
    ],
)
def test_sorted_pluck(data, many, result):
    """Test if the custom "SortedPluck" field works correctly."""
    assert (
        SortedPluck(TagSchema, "name", many=many)._serialize(data, "name", None)
        == result
    )


def test_check_duplicate_identifier(dummy_record):
    """Test if checking for a duplicate identifier works correctly."""
    check_duplicate_identifier(Record, "test")
    check_duplicate_identifier(Record, dummy_record.identifier, exclude=dummy_record)

    with pytest.raises(ValidationError):
        check_duplicate_identifier(Record, dummy_record.identifier)
