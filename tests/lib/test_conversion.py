# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from datetime import datetime

import pytest

from kadi.lib.conversion import clamp
from kadi.lib.conversion import lower
from kadi.lib.conversion import normalize
from kadi.lib.conversion import parse_boolean_string
from kadi.lib.conversion import parse_datetime_string
from kadi.lib.conversion import parse_json_object
from kadi.lib.conversion import recode_string
from kadi.lib.conversion import strip
from kadi.lib.conversion import strip_markdown
from kadi.lib.conversion import truncate


@pytest.mark.parametrize(
    "value,result",
    [
        (None, None),
        (" test ", "test"),
        ([" test ", " foo  bar "], ["test", "foo  bar"]),
    ],
)
def test_strip(value, result):
    """Test if surrounding whitespaces in strings are stripped correctly."""
    assert strip(value) == result


@pytest.mark.parametrize(
    "value,result",
    [
        (None, None),
        (" foo  bar ", "foo bar"),
        ([" test ", " foo  bar "], ["test", "foo bar"]),
    ],
)
def test_normalize(value, result):
    """Test if whitespaces in string are normalized correctly."""
    assert normalize(value) == result


@pytest.mark.parametrize(
    "value,result",
    [
        (None, None),
        ("TEST ", "test "),
        (["TEST ", "FOO BAR"], ["test ", "foo bar"]),
    ],
)
def test_lower(value, result):
    """Test if strings are lowered correctly."""
    assert lower(value) == result


@pytest.mark.parametrize(
    "value,length,result",
    [
        (None, 1, None),
        ("test", 0, "..."),
        ("test", 1, "t..."),
        ("test", 4, "test"),
        ("test", 5, "test"),
        (["test", "foo bar"], 4, ["test", "foo ..."]),
    ],
)
def test_truncate(value, length, result):
    """Test if strings are truncated correctly."""
    assert truncate(value, length) == result


@pytest.mark.parametrize(
    "value,from_enc,to_enc,result",
    [
        (None, "utf-8", "utf-8", None),
        ("test", "utf-8", "ascii", b"test"),
        (b"\xc3\x83\xc2\xb6".decode(), "latin-1", "utf-8", b"\xc3\xb6"),
    ],
)
def test_recode_string(value, from_enc, to_enc, result):
    """Test if strings are recoded correctly."""
    recoded_string = recode_string(value, from_encoding=from_enc, to_encoding=to_enc)

    if result is not None:
        assert recoded_string.encode() == result
    else:
        assert recoded_string is None


@pytest.mark.parametrize("value,result", [(0, 1), (1, 1), (2, 2), (10, 10), (11, 10)])
def test_clamp(value, result):
    """Test if values are clamped correctly."""
    assert clamp(value, 1, 10) == result


@pytest.mark.parametrize(
    "value,is_valid",
    [
        ("test", False),
        ("2020-01-01T12:34:56Z", True),
        ("2020-01-01T12:34:56+00:00", True),
        ("2020-01-01T12:34:56.0000Z", True),
        ("2020-01-01T12:34:56.00000+00:00", True),
    ],
)
def test_parse_datetime_string(value, is_valid):
    """Test if datetime strings are parsed correctly."""
    if is_valid:
        dt = datetime.fromisoformat("2020-01-01T12:34:56.000000+00:00")
        assert parse_datetime_string(value) == dt
    else:
        assert parse_datetime_string(value) is None


@pytest.mark.parametrize(
    "value,result",
    [
        (True, True),
        ("TRUE", True),
        ("t", True),
        ("Yes", True),
        ("Y", True),
        ("On", True),
        ("1", True),
        ("FALSE", False),
        ("test", False),
        ("No", False),
        ("N", False),
        ("0", False),
        (0, False),
        (False, False),
    ],
)
def test_parse_boolean_string(value, result):
    """Test if boolean strings are parsed correctly."""
    assert parse_boolean_string(value) is result


@pytest.mark.parametrize(
    "value,result",
    [(None, {}), ("{", {}), ('{"test": true}', {"test": True})],
)
def test_parse_json_object(value, result):
    """Test if JSON object strings are parsed correctly."""
    assert parse_json_object(value) == result


@pytest.mark.parametrize(
    "value,result",
    [
        (None, None),
        ("# a", "a"),
        ("**a**", "a"),
        ("*a*", "a"),
        ("~~a~~", "a"),
        ("`a`", "a"),
        ("$a$", "a"),
        ("1. a", "a"),
        ("* a", "a"),
        ("---", ""),
        ("|a|b|\n|-|-|\n|1|2|", "a b 1 2"),
        ('[text](https://example.com "title")', "text"),
        ('![alt text](https://example.com "title")', ""),
        # Currently not supported by the markdown renderer.
        ("a^2^", "a^2^"),
        ("a~2~", "a~2~"),
        # Existing HTML should be left intact.
        ("*<b>a</b>*", "<b>a</b>"),
    ],
)
def test_strip_markdown(value, result):
    """Test if markdown is stripped correctly from strings."""
    assert strip_markdown(value) == result
