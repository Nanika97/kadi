# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from datetime import datetime
from datetime import timezone

import pytest
from sqlalchemy.exc import IntegrityError
from sqlalchemy.types import Text

from kadi.lib.db import check_constraint
from kadi.lib.db import composite_index
from kadi.lib.db import escape_like
from kadi.lib.db import get_class_by_tablename
from kadi.lib.db import get_class_of_relationship
from kadi.lib.db import get_column_type
from kadi.lib.db import has_extension
from kadi.lib.db import has_pending_revisions
from kadi.lib.db import is_column
from kadi.lib.db import is_many_relationship
from kadi.lib.db import is_relationship
from kadi.lib.db import length_constraint
from kadi.lib.db import NestedTransaction
from kadi.lib.db import range_constraint
from kadi.lib.db import unique_constraint
from kadi.lib.db import values_constraint
from kadi.lib.resources.utils import add_link
from kadi.lib.resources.utils import remove_link
from kadi.modules.accounts.models import User
from kadi.modules.collections.models import Collection
from kadi.modules.collections.models import CollectionState
from kadi.modules.records.models import File
from kadi.modules.records.models import Record
from kadi.modules.records.models import RecordState


def test_utc_date_time(db, dummy_record):
    """Test if the custom "UTCDateTime" type for dates works correctly."""
    current_local_time = datetime.now()
    dummy_record.created_at = current_local_time

    db.session.commit()
    db.session.refresh(dummy_record)

    assert dummy_record.created_at == current_local_time.astimezone(timezone.utc)
    assert dummy_record.created_at.tzinfo == timezone.utc


def test_simple_timestamp_mixin(db, dummy_user, new_record):
    """Test if the "SimpleTimestampMixin" works correctly."""

    # Check if the timestamp gets updated when manually updating it.
    prev_timestamp = dummy_user.last_modified
    dummy_user.update_timestamp()

    assert dummy_user.last_modified != prev_timestamp

    # Check if the timestamp gets updated when changing an attribute that is not
    # ignored.
    prev_timestamp = dummy_user.last_modified
    dummy_user.about = "test"
    db.session.commit()

    assert dummy_user.last_modified != prev_timestamp

    # Check if the timestamp stays unchanged when changing an excluded relationship by
    # creating a new record with the dummy user as creator.
    prev_timestamp = dummy_user.last_modified
    new_record()
    db.session.commit()

    assert dummy_user.last_modified == prev_timestamp


def test_state_timestamp_mixin(
    db, dummy_collection, dummy_record, dummy_user, new_upload
):
    """Test if the "StateTimestampMixin" works correctly."""

    # Check if the timestamp gets updated when manually updating it.
    prev_timestamp = dummy_record.last_modified
    dummy_record.update_timestamp()

    assert dummy_record.last_modified != prev_timestamp

    # Check if the timestamp gets updated when changing an attribute that is not
    # ignored.
    prev_timestamp = dummy_user.last_modified
    dummy_record.title = "test"
    db.session.commit()

    assert dummy_record.last_modified != prev_timestamp

    # Check if the timestamp gets updated when changing a relationship that is not
    # ignored.
    prev_timestamp = dummy_record.last_modified
    add_link(dummy_record.collections, dummy_collection, user=dummy_user)
    db.session.commit()

    assert dummy_record.last_modified != prev_timestamp

    # Check if the timestamp stays unchanged when changing a relationship that is
    # referencing a deleted resource.
    prev_timestamp = dummy_record.last_modified
    dummy_collection.state = CollectionState.DELETED
    remove_link(dummy_record.collections, dummy_collection, user=dummy_user)
    db.session.commit()

    assert dummy_record.last_modified == prev_timestamp

    # Check if the timestamp stays unchanged when changing a relationship that is
    # ignored by creating a new upload related to the dummy record.
    new_upload()
    db.session.commit()

    assert dummy_record.last_modified == prev_timestamp

    # Check if the timestamp gets updated when marking the record as deleted.
    dummy_record.state = RecordState.DELETED
    db.session.commit()

    assert dummy_record.last_modified != prev_timestamp

    # Check if the timestamp now stays unchanged when trying to update it manually.
    prev_timestamp = dummy_record.last_modified
    dummy_record.update_timestamp()

    assert dummy_record.last_modified == prev_timestamp

    # Check if the timestamp stays unchanged when changing an attribute that is not
    # ignored, but the record is marked as deleted.
    dummy_record.identifier = "test"
    db.session.commit()

    assert dummy_record.last_modified == prev_timestamp

    # Check if the timestamp gets updated again when marking the record as active.
    dummy_record.state = RecordState.ACTIVE
    db.session.commit()

    assert dummy_record.last_modified != prev_timestamp


def test_nested_transaction(db, dummy_record, new_record):
    """Test if nested transactions work correctly."""
    new_title = "test"
    old_identifier = dummy_record.identifier

    # Change the title of the dummy record, which should succeed.
    with NestedTransaction() as t1:
        dummy_record.title = new_title

        # Change the identifier of the dummy record to an already existing one, which
        # should not succeed.
        with NestedTransaction(exc=IntegrityError) as t2:
            dummy_record.identifier = new_record().identifier

        assert not t2.success

    db.session.commit()

    assert t1.success
    assert dummy_record.title == new_title
    assert dummy_record.identifier == old_identifier


def test_composite_index():
    """Test if composite indices are generated correctly."""
    assert composite_index("table", "test1", "test2").name == "ix_table_test1_test2"


def test_unique_constraint():
    """Test if unique constraints are generated correctly."""
    assert unique_constraint("table", "test1", "test2").name == "uq_table_test1_test2"


def test_check_constraint():
    """Test if check constraints are generated correctly."""
    assert check_constraint("constraint", "test").name == "ck_test"


def test_length_constraint():
    """Test if length constraints are generated correctly."""
    constraint = length_constraint("test", min_value=0, max_value=1)

    assert constraint.name == "ck_test_length"
    assert (
        str(constraint.sqltext) == "char_length(test) >= 0 AND char_length(test) <= 1"
    )


def test_range_constraint():
    """Test if range constraints are generated correctly."""
    constraint = range_constraint("test", min_value=0, max_value=1)

    assert constraint.name == "ck_test_range"
    assert str(constraint.sqltext) == "test >= 0 AND test <= 1"


def test_values_constraint():
    """Test if value constraints are generated correctly."""
    constraint = values_constraint("test", ["test1", "test2"])

    assert constraint.name == "ck_test_values"
    assert str(constraint.sqltext) == "test IN ('test1', 'test2')"


@pytest.mark.parametrize("tablename,cls", [("record", Record), ("test", None)])
def test_get_class_by_tablename(tablename, cls):
    """Test if determining a class by tablename works correctly."""
    assert get_class_by_tablename(tablename) == cls


@pytest.mark.parametrize(
    "attr,result",
    [
        ("identifier", True),
        ("user_id", True),
        ("creator", False),
    ],
)
def test_is_column(attr, result):
    """Test if columns of a model are determined correctly."""
    assert is_column(Record, attr) is result


@pytest.mark.parametrize("attr,col_type", [("identifier", Text), ("files", type(None))])
def test_get_column_type(attr, col_type):
    """Test if determining the column type of a model works correctly."""
    assert isinstance(get_column_type(Record, attr), col_type)


@pytest.mark.parametrize(
    "attr,result",
    [
        ("creator", True),
        ("files", True),
        ("collections", True),
        ("identifier", False),
        ("user_id", False),
    ],
)
def test_is_relationship(attr, result):
    """Test if relationships of a model are determined correctly."""
    assert is_relationship(Record, attr) is result


@pytest.mark.parametrize(
    "attr,result",
    [
        ("files", True),
        ("collections", True),
        ("creator", False),
        ("identifier", False),
        ("user_id", False),
    ],
)
def test_is_many_relationship(attr, result):
    """Test if many-relationships of a model are determined correctly."""
    assert is_many_relationship(Record, attr) is result


@pytest.mark.parametrize(
    "attr,cls",
    [
        ("identifier", None),
        ("creator", User),
        ("files", File),
        ("collections", Collection),
    ],
)
def test_get_class_of_relationship(attr, cls):
    """Test if determining the class of a relationship works correctly."""
    assert get_class_of_relationship(Record, attr) == cls


@pytest.mark.parametrize(
    "value,escape,expected",
    [
        ("test", "\\", "test"),
        ("test%_\\", "\\", "test\\%\\_\\\\"),
        ("test%_/", "/", "test/%/_//"),
    ],
)
def test_escape_like(value, escape, expected):
    """Test if strings for use in LIKE queries are escaped correctly."""
    assert escape_like(value, escape=escape) == expected


@pytest.mark.parametrize(
    "extension,result",
    [
        ("invalid_extension", False),
        # "plpgsql" is installed by default in PostgreSQL 9.0 and later.
        ("plpgsql", True),
    ],
)
def test_has_extension(extension, result):
    """Test if checking for database extensions works correctly."""
    assert has_extension(extension) is result


def test_has_pending_revisions():
    """Test if checking for pending revisions works correctly."""
    assert not has_pending_revisions()
