# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from datetime import timedelta

import pytest

from kadi.lib.api.models import PersonalToken
from kadi.lib.utils import utcnow


def test_personal_token_is_expired(dummy_user):
    """Test if the expiration date of personal tokens works correctly."""
    personal_token = PersonalToken.create(user=dummy_user, name="test")
    assert not personal_token.is_expired

    personal_token.expires_at = utcnow() - timedelta(seconds=1)
    assert personal_token.is_expired


@pytest.mark.parametrize("include_prefix", [True, False])
def test_personal_token_get_by_token(include_prefix, dummy_user):
    """Test if retrieving personal tokens by token value works correctly."""
    token = PersonalToken.new_token(include_prefix=include_prefix)
    personal_token = PersonalToken.create(user=dummy_user, name="test", token=token)

    assert PersonalToken.get_by_token(token) == personal_token


@pytest.mark.parametrize("include_prefix", [True, False])
def test_personal_token_create(include_prefix, dummy_user):
    """Test if creating new personal tokens works correctly."""
    token = PersonalToken.new_token(include_prefix=include_prefix)
    personal_token = PersonalToken.create(user=dummy_user, name="test", token=token)

    assert PersonalToken.hash_token(token) == personal_token.token_hash
