# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import current_app

import kadi.lib.constants as const
from kadi.lib.web import make_next_url
from tests.utils import check_api_response
from tests.utils import check_view_response
from tests.utils import get_cookie


@pytest.mark.parametrize(
    "endpoint,method,status_code",
    [("/api/404", "get", 404), ("/api/records", "put", 405)],
)
def test_app_errorhandler_api_authenticated(
    endpoint, method, status_code, api_client, dummy_personal_token
):
    """Test if the app error handler works for authenticated API requests."""
    response = getattr(api_client(dummy_personal_token), method)(endpoint)
    check_api_response(response, status_code=status_code)
    # Check for any of the Flask-talisman headers.
    assert "Content-Security-Policy" in response.headers


@pytest.mark.parametrize(
    "endpoint,method", [("/api/404", "get"), ("/api/records", "put")]
)
def test_app_errorhandler_api_unauthenticated(endpoint, method, api_client):
    """Test if the app error handler works for unauthenticated API requests."""
    response = getattr(api_client("test"), method)(endpoint)
    check_api_response(response, status_code=401)
    # Check for any of the Flask-talisman headers.
    assert "Content-Security-Policy" in response.headers


@pytest.mark.parametrize(
    "endpoint,method,status_code",
    [("/404", "get", 404), ("/records/new", "put", 405)],
)
def test_app_errorhandler_views_authenticated(
    endpoint, method, status_code, client, user_session
):
    """Test if the app error handler works for authenticated non-API requests."""
    with user_session():
        response = getattr(client, method)(endpoint)

        check_view_response(response, status_code=status_code)
        # Check for any of the Flask-talisman headers.
        assert "Content-Security-Policy" in response.headers


@pytest.mark.parametrize(
    "endpoint,method",
    [("/404", "get"), ("/records/new", "put")],
)
def test_app_errorhandler_views(endpoint, method, client):
    """Test if the app error handler works for unauthenticated non-API requests."""
    response = getattr(client, method)(endpoint)

    check_view_response(response, status_code=302)
    assert response.location == make_next_url(endpoint)
    # Check for any of the Flask-talisman headers.
    assert "Content-Security-Policy" in response.headers


def test_after_app_request_locale_cookie(client):
    """Test if the locale cookie is set correctly."""
    locale_cookie_name = current_app.config["LOCALE_COOKIE_NAME"]

    for locale in const.LOCALES:
        client.cookie_jar.clear()

        # Set the initial locale cookie.
        client.get("/")
        cookie = get_cookie(client, locale_cookie_name)

        assert cookie
        assert cookie.value == const.LOCALE_DEFAULT

        # Set the locale cookie using the request argument.
        client.get(f"/?locale={locale}")
        cookie = get_cookie(client, locale_cookie_name)

        assert cookie
        assert cookie.value == locale

        # Set the previous locale cookie.
        client.get("/")
        cookie = get_cookie(client, locale_cookie_name)

        assert cookie
        assert cookie.value == locale
