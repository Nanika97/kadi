# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os

from flask_login import current_user

from kadi.lib.storage.misc import create_misc_storage
from kadi.lib.web import url_for
from kadi.modules.accounts.models import User
from kadi.modules.accounts.models import UserState
from kadi.modules.accounts.utils import clean_users
from kadi.modules.accounts.utils import delete_user_image
from kadi.modules.accounts.utils import get_filtered_user_ids
from kadi.modules.accounts.utils import login_user
from kadi.modules.accounts.utils import logout_user
from kadi.modules.accounts.utils import save_user_image


def test_login_logout_user(dummy_user, request_context):
    """Test if logging a user in and out works correctly."""
    assert not current_user.is_authenticated

    login_user(dummy_user.identity)

    assert current_user.is_authenticated

    redirect_url = logout_user()

    assert not current_user.is_authenticated
    assert redirect_url == url_for("main.index")


def test_save_user_image(dummy_image, dummy_user):
    """Test if saving a user profile image works correctly."""
    storage = create_misc_storage()

    save_user_image(dummy_user, dummy_image)
    first_image_name = dummy_user.image_name

    assert first_image_name is not None
    assert storage.exists(storage.create_filepath(str(first_image_name)))

    dummy_image.seek(0)
    save_user_image(dummy_user, dummy_image)
    second_image_name = dummy_user.image_name

    assert second_image_name is not None
    assert not storage.exists(storage.create_filepath(str(first_image_name)))
    assert storage.exists(storage.create_filepath(str(second_image_name)))


def test_delete_user_image(dummy_image, dummy_user):
    """Test if deleting a user profile image works correctly."""
    storage = create_misc_storage()

    save_user_image(dummy_user, dummy_image)
    delete_user_image(dummy_user)

    assert dummy_user.image_name is None
    assert not os.listdir(storage.root_directory)


def test_get_filtered_user_ids(dummy_user):
    """Test if users are filtered correctly."""
    assert not get_filtered_user_ids("test").all()
    assert get_filtered_user_ids(dummy_user.identity.username).one().id == dummy_user.id
    assert (
        get_filtered_user_ids(dummy_user.identity.displayname).one().id == dummy_user.id
    )


def test_clean_users(dummy_user):
    """Test if cleaning users works correctly."""
    clean_users()

    assert User.query.one()

    dummy_user.state = UserState.DELETED
    clean_users()

    assert not User.query.all()
