# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from flask import json
from werkzeug.datastructures import MultiDict

import kadi.lib.constants as const
from kadi.lib.forms import KadiForm
from kadi.modules.settings.forms import HomeLayoutField
from kadi.modules.settings.forms import RedirectURIsField
from kadi.modules.settings.forms import ScopesField


@pytest.mark.parametrize(
    "data,is_valid,result",
    [
        (None, True, const.USER_CONFIG_HOME_LAYOUT_DEFAULT),
        (
            {"test": json.dumps(const.USER_CONFIG_HOME_LAYOUT_DEFAULT)},
            True,
            const.USER_CONFIG_HOME_LAYOUT_DEFAULT,
        ),
        ({"test": "[]"}, True, []),
        ({"test": "[{}]"}, False, "Invalid data structure."),
    ],
)
def test_home_layout_field(data, is_valid, result):
    """Test if the custom "HomeLayoutField" works correctly."""

    class _TestForm(KadiForm):
        test = HomeLayoutField("Test")

    form = _TestForm(formdata=MultiDict(data))

    assert form.validate() is is_valid

    if is_valid:
        assert form.test.data == result
    else:
        assert form.test.data == const.USER_CONFIG_HOME_LAYOUT_DEFAULT
        assert result in form.errors["test"]


@pytest.mark.parametrize(
    "data,is_valid,result",
    [
        (None, True, None),
        ({"test": ""}, True, None),
        ({"test": "foo.bar"}, False, "One or more scopes are invalid."),
        (
            {"test": "record.read collection.read record.read"},
            True,
            "collection.read record.read",
        ),
    ],
)
def test_scopes_field(data, is_valid, result):
    """Test if the custom "ScopesField" works correctly."""

    class _TestForm(KadiForm):
        test = ScopesField("Test")

    form = _TestForm(formdata=MultiDict(data))

    assert form.validate() is is_valid

    if is_valid:
        assert form.test.data == result
    else:
        assert result in form.errors["test"]


@pytest.mark.parametrize(
    "data,is_valid,result",
    [
        (None, True, []),
        ({"test": ""}, True, []),
        ({"test": "https://localhost"}, True, ["https://localhost"]),
        (
            {"test": "http://localhost:5000\r\nhttps://example.com"},
            True,
            ["http://localhost:5000", "https://example.com"],
        ),
        ({"test": "http://127.0.0.1"}, False, None),
        ({"test": "http://example.com"}, False, None),
        ({"test": "file://test.txt"}, False, None),
        ({"test": "https://"}, False, None),
        ({"test": "test"}, False, None),
        ({"test": f"https://{'x' * 2049}"}, False, None),
    ],
)
def test_redirect_uris_field(data, is_valid, result):
    """Test if the custom "RedirectURIsField" works correctly."""

    class _TestForm(KadiForm):
        test = RedirectURIsField("Test")

    form = _TestForm(formdata=MultiDict(data))

    assert form.validate() is is_valid

    if is_valid:
        assert form.test.data == result
    else:
        assert "One or more redirect URIs are invalid." in form.errors["test"]
