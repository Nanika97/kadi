{# Copyright 2023 Karlsruhe Institute of Technology
 #
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 #
 #     http://www.apache.org/licenses/LICENSE-2.0
 #
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License. #}

{% extends "settings/base.html" %}

{% block breadcrumbs %}
  {{ super() }}
  {% snippet "breadcrumb", url=url_for("settings.manage_applications"), title=title %}
{% endblock %}

{% block content %}
  <div class="card">
    {% include "settings/snippets/menu.html" %}

    <div class="card-body">
      <div class="card bg-light">
        <div class="card-body">
          {% if client %}
            <p>
              <i class="fa-solid fa-circle-info"></i>
              {% trans %}
                A new OAuth2 application has been registered. Please keep the client secret somewhere safe as you will
                not be able to access it again.
              {% endtrans %}
            </p>
            <div class="row">
              <div class="col-lg-5 mb-2 mb-lg-0">
                <strong>{% trans %}Client ID{% endtrans %}</strong>
                <div class="input-group input-group-sm">
                  <input class="form-control" readonly="true" value="{{ client.client_id }}">
                  <div class="input-group-append">
                    <clipboard-button content="{{ client.client_id }}" class="input-group-text"></clipboard-button>
                  </div>
                </div>
              </div>
              <div class="col-lg-7">
                <strong>{% trans %}Client secret{% endtrans %}</strong>
                <div class="input-group input-group-sm">
                  <input class="form-control" readonly="true" value="{{ client_secret }}">
                  <div class="input-group-append">
                    <clipboard-button content="{{ client_secret }}" class="input-group-text"></clipboard-button>
                  </div>
                </div>
              </div>
            </div>
          {% else %}
            {% set help_url = '<a class="text-primary" target="_blank" rel="noopener noreferrer" href="{}">'
                              '<strong>{}</strong>'
                              '</a>'
                              .format(url_for("main.help", _anchor="settings"), _("Help")) %}

            <i class="fa-solid fa-circle-info"></i>
            {% trans help=help_url | safe %}
              Applications can be used to integrate another service with the HTTP API that Kadi4Mat provides. Please see
              the {{ help }} page for more information.
            {% endtrans %}
          {% endif %}
        </div>
      </div>
      <hr>
      <submission-form>
        {{ render_csrf() }}

        <div class="row">
          <text-field class="col-lg" :field="{{ form.client_name }}"></text-field>
          <text-field class="col-lg" :field="{{ form.client_uri }}"></text-field>
        </div>
        <textarea-field :field="{{ form.redirect_uris }}" :rows="3"></textarea-field>
        <scopes-field :field="{{ form.scope }}" :scopes="kadi.context.scopes"></scopes-field>
        <submit-field :field="{{ form.submit }}"></submit-field>
      </submission-form>
      <hr>
      <dynamic-pagination endpoint="{{ url_for('api.get_registered_applications') }}"
                          placeholder="{% trans %}No applications.{% endtrans %}"
                          :per-page="5">
        <template #default="props">
          <p>
            <strong>{% trans %}Registered applications{% endtrans %}</strong>
            <span class="badge badge-pill badge-light text-muted border border-muted">{$ props.total $}</span>
          </p>
          <ul class="list-group" v-if="props.total > 0">
            <li class="list-group-item py-2 bg-light">
              <div class="row">
                <div class="col-md-2">{% trans %}Name{% endtrans %}</div>
                <div class="col-md-3">{% trans %}Created at{% endtrans %}</div>
                <div class="col-md-2">{% trans %}Client ID{% endtrans %}</div>
                <div class="col-md-4">{% trans %}Scopes{% endtrans %}</div>
              </div>
            </li>
            <li class="list-group-item" v-for="application in props.items" :key="application.id">
              <div class="row align-items-center mb-2">
                <div class="col-md-2 mb-2 mb-md-0">
                  <strong>{$ application.client_name $}</strong>
                </div>
                <div class="col-md-3 mb-2 mb-md-0">
                  <local-timestamp :timestamp="application.created_at"></local-timestamp>
                  <br>
                  <small class="text-muted">
                    (<from-now :timestamp="application.created_at"></from-now>)
                  </small>
                </div>
                <div class="col-md-2 mb-2 mb-md-0">
                  <pre class="ws-pre-wrap mb-0">{$ application.client_id $}</pre>
                </div>
                <div class="col-md-3 mb-2 mb-md-0">
                  <scopes-viewer :scope="application.scope"></scopes-viewer>
                </div>
                <div class="col-md-2 d-md-flex justify-content-end">
                  <a class="btn btn-sm btn-light"
                     title="{% trans %}Edit application{% endtrans %}"
                     :href="application._links.edit">
                    <i class="fa-solid fa-pencil"></i> {% trans %}Edit{% endtrans %}
                  </a>
                </div>
              </div>
              <small>
                <i class="fa-solid fa-arrow-up-right-from-square mr-1"></i>
                <a :href="application._links.authorize" target="_blank" rel="noopener noreferrer">
                  <strong>{% trans %}Example authorization request{% endtrans %}</strong>
                </a>
              </small>
            </li>
          </ul>
        </template>
      </dynamic-pagination>
      <hr>
      <dynamic-pagination endpoint="{{ url_for('api.get_authorized_applications') }}"
                          placeholder="{% trans %}No applications.{% endtrans %}"
                          :per-page="5"
                          ref="pagination">
        <template #default="props">
          <p>
            <strong>{% trans %}Authorized applications{% endtrans %}</strong>
            <span class="badge badge-pill badge-light text-muted border border-muted">{$ props.total $}</span>
          </p>
          <ul class="list-group" v-if="props.total > 0">
            <li class="list-group-item py-2 bg-light">
              <div class="row">
                <div class="col-md-2">{% trans %}Name{% endtrans %}</div>
                <div class="col-md-5">{% trans %}Website{% endtrans %}</div>
                <div class="col-md-4">{% trans %}Scopes{% endtrans %}</div>
              </div>
            </li>
            <li class="list-group-item" v-for="application in props.items" :key="application.id">
              <div class="row align-items-center">
                <div class="col-md-2 mb-2 mb-md-0">
                  <strong>{$ application.client.client_name $}</strong>
                </div>
                <div class="col-md-5 mb-2 mb-md-0">
                  <a :href="application.client.client_uri" target="_blank" rel="noopener noreferrer">
                    <i class="fa-solid fa-arrow-up-right-from-square fa-sm mr-1"></i>
                    <strong>{$ application.client.client_uri $}</strong>
                  </a>
                </div>
                <div class="col-md-4 mb-2 mb-md-0">
                  <scopes-viewer :scope="application.scope"></scopes-viewer>
                </div>
                <div class="col-md-1 d-md-flex justify-content-end">
                  <button type="button"
                          class="btn btn-sm btn-danger"
                          title="{% trans %}Revoke access{% endtrans %}"
                          :disabled="application.disabled"
                          @click="revokeAccess(application)">
                    <i class="fa-solid fa-trash"></i>
                  </button>
                </div>
              </div>
            </li>
          </ul>
        </template>
      </dynamic-pagination>
    </div>
  </div>
{% endblock %}

{% block scripts %}
  <script nonce="{{ csp_nonce() }}">
    kadi.base.newVue({
      methods: {
        revokeAccess(application) {
          if (!window.confirm('{{ _("Are you sure you want to revoke access of this application?") }}')) {
            return;
          }

          this.$set(application, 'disabled', true);

          axios.delete(application._actions.remove)
            .then(() => {
              this.$refs.pagination.update();
              kadi.base.flashSuccess('{{ _("Access revoked successfully.") }}', {scrollTo: false});
            })
            .catch((error) => {
              kadi.base.flashDanger('{{ _("Error revoking access.") }}', {request: error.request});
              application.disabled = false;
            });
        },
      },
    });
  </script>
{% endblock %}
